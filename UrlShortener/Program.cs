﻿using UrlShortener.Services;
using UrlShortener.Settings;
using WebMarkupMin.AspNetCore6;

namespace UrlShortener;

public static class Program
{
	private static DatabaseSettings parseDatabaseSettings(IConfigurationSection config)
	{
		DatabaseType type = config.GetValue<DatabaseType>("Type");

		switch (type)
		{
			case DatabaseType.Legacy:
			{
				LegacyDatabaseSettings settings = new();
				config.Bind(settings);
				return settings;
			}

			case DatabaseType.Simple:
			{
				SimpleDatabaseSettings settings = new();
				config.Bind(settings);
				return settings;
			}

			case DatabaseType.Combined:
			{
				CombinedDatabaseSettings settings = new()
				{
					Primary = parseDatabaseSettings(config.GetSection("Primary")),
					Secondary = parseDatabaseSettings(config.GetSection("Secondary"))
				};
				return settings;
			}

			default:
				throw new ArgumentOutOfRangeException();
		}
	}

	public static async Task Main(string[] args)
	{
		if (args.Length == 2 && args[0] == "generate-password")
		{
			ShortenerPassword pw = new ShortenerPassword(args[1]);

			Console.WriteLine("{");
			Console.WriteLine($"\t\"Password\": \"{pw.Password}\",");
			Console.WriteLine($"\t\"Salt\": \"{pw.Salt}\",");
			Console.WriteLine($"\t\"IterationCount\": \"{pw.IterationCount}\",");
			Console.WriteLine($"\t\"Prf\": \"{pw.Prf}\"");
			Console.WriteLine("}");

			return;
		}

		WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

		// Load settings.
		ShortenerSettings settings = new();
		IConfigurationSection? settingsSection = builder.Configuration.GetSection("Shortener");
		settingsSection.Bind(settings);

		// Configure DB.
		DatabaseSettings dbSettings = parseDatabaseSettings(settingsSection.GetSection("Database"));
		IShortUrlDatabase db = await dbSettings.BuildAsync(settings);

		builder.Services.Configure<ShortenerSettings>(settingsSection);
		builder.Services.AddSingleton(db);
		builder.Services.AddControllersWithViews();
		builder.Services.AddWebMarkupMin()
			.AddHtmlMinification();

		WebApplication app = builder.Build();

		if (app.Environment.IsDevelopment())
		{
			app.UseDeveloperExceptionPage();
		}
		else
		{
			app.UseExceptionHandler("/Home/Error");
		}

		app.UseWebMarkupMin();
		app.UseStaticFiles();
		app.UseRouting();
		app.UseStatusCodePages();

		app.UseEndpoints(endpoints => { endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}"); });

		await app.RunAsync();
	}
}