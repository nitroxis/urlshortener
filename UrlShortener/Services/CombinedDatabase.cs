﻿namespace UrlShortener.Services;

public class CombinedDatabase : IShortUrlDatabase
{
	private readonly IShortUrlDatabase primary;
	private readonly IShortUrlDatabase secondary;

	public CombinedDatabase(IShortUrlDatabase primary, IShortUrlDatabase secondary)
	{
		this.primary = primary;
		this.secondary = secondary;
	}

	public async ValueTask<string?> ResolveAsync(string shortUrlId)
	{
		return await this.primary.ResolveAsync(shortUrlId) ?? await this.secondary.ResolveAsync(shortUrlId);
	}

	public async ValueTask<string> AddAsync(string longUrl)
	{
		return await this.primary.AddAsync(longUrl);
	}
}