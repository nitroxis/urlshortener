﻿namespace UrlShortener.Services;

public interface IShortUrlDatabase
{
	ValueTask<string?> ResolveAsync(string shortUrlId);

	ValueTask<string> AddAsync(string longUrl);
}