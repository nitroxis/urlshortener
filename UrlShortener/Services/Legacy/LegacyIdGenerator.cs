﻿namespace UrlShortener.Services.Legacy;

public static class LegacyIdGenerator
{
	#region Fields

	public const int Characters = 5;
	private const int rounds = 10;
	private const int bits = Characters * 6;
	private const int bitsHalf = bits / 2;
	private const ulong lowMask = ((1ul << bitsHalf) - 1);

	#endregion

	#region Methods

	private static ulong f(ulong n)
	{
		return (((n ^ 1337) + 42) << 1) & lowMask;
	}

	/// <summary>
	/// Permutes an integer.
	/// </summary>
	/// <param name="n"></param>
	/// <returns></returns>
	public static ulong Permute(ulong n)
	{
		ulong high = n >> bitsHalf;
		ulong low = n & lowMask;

		for (int i = 0; i < rounds; ++i)
		{
			high ^= f(low);
			(high, low) = (low, high);
		}

		return high | (low << bitsHalf);
	}

	#endregion
}