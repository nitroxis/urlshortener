﻿namespace UrlShortener.Services.Legacy;

public static class Base64
{
	private static string table = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_-";

	public static ulong Decode(string str)
	{
		ulong n = 0;

		for (int i = 0; i < str.Length; ++i)
		{
			n = (n << 6) | (uint)table.IndexOf(str[i]);
		}

		return n;
	}

	public static string Encode(ulong n, int numChars)
	{
		char[] chars = new char[numChars];
		for (int i = chars.Length - 1; i >= 0; --i)
		{
			chars[i] = table[(int)(n & 63)];
			n >>= 6;
		}
		return new string(chars);
	}
}