﻿namespace UrlShortener.Services.Legacy;

public class LegacyDatabase : IShortUrlDatabase
{
	private readonly TextDatabase db;

	public LegacyDatabase(TextDatabase db)
	{
		this.db = db;
	}

	public async ValueTask<string?> ResolveAsync(string shortUrlId)
	{
		ulong permutedId = Base64.Decode(shortUrlId);
		int id = (int)LegacyIdGenerator.Permute(permutedId);
		return await this.db.GetAsync(id);
	}

	public async ValueTask<string> AddAsync(string longUrl)
	{
		int id = await this.db.AddAsync(longUrl);
		ulong permutedId = LegacyIdGenerator.Permute((ulong)id);
		string str = Base64.Encode(permutedId, 5);
		return str;
	}
}