﻿using System.Buffers;
using System.Diagnostics;
using System.IO.Pipelines;
using System.Text;
using Nito.AsyncEx;

namespace UrlShortener.Services;

public readonly record struct TextDatabaseRange(long Offset, int Length);

public readonly record struct TextDatabaseHash(int Hash, int Index);

/// <summary>
/// Simple, text-based URL database where each line is one entry.
/// </summary>
public sealed class TextDatabase : IDisposable
{
	#region Fields

	private static readonly Encoding encoding = new UTF8Encoding(false, true);
	private static readonly byte[] newLine = { (byte)'\n' };

	private readonly AsyncReaderWriterLock rwLock;
	private readonly List<TextDatabaseRange> lines;
	private readonly List<TextDatabaseHash> hashTable;
	private readonly int maxUriLength;
	private readonly Stream stream;

	#endregion

	#region Properties

	public int Count => this.lines.Count;

	#endregion

	#region Constructors

	public TextDatabase(Stream stream, int maxUriLength)
	{
		this.rwLock = new();
		this.lines = new();
		this.hashTable = new();
		this.maxUriLength = maxUriLength;
		this.stream = stream;
	}

	#endregion

	#region Methods

	public static async Task<TextDatabase> ParseAsync(Stream stream, int maxUriLength)
	{
		TextDatabase db = new(stream, maxUriLength);

		long offset = 0;

		void processLine(ReadOnlySequence<byte> bytes)
		{
			int len = (int)(bytes.Length);
			string url = Encoding.UTF8.GetString(bytes);

			db.addEntry(offset, len, url.GetHashCode());

			offset += bytes.Length + 1; // Remember the newline.
		}

		PipeReader reader = PipeReader.Create(stream, new StreamPipeReaderOptions(leaveOpen: true));
		while (true)
		{
			ReadResult result = await reader.ReadAsync();

			ReadOnlySequence<byte> buffer = result.Buffer;
			SequencePosition? position;

			do
			{
				// Look for a EOL in the buffer
				position = buffer.PositionOf((byte)'\n');

				if (position != null)
				{
					// Process the line
					processLine(buffer.Slice(0, position.Value));

					// Skip the line + the \n character (basically position)
					buffer = buffer.Slice(buffer.GetPosition(1, position.Value));
				}
			} while (position != null);

			// Tell the PipeReader how much of the buffer we have consumed
			reader.AdvanceTo(buffer.Start, buffer.End);

			// Stop reading if there's no more data coming
			if (result.IsCompleted)
			{
				if (buffer.Length > 0)
				{
					// Missing newline at the end of the file.
					processLine(buffer);

					// Add new line.
					await stream.WriteAsync(newLine);
				}

				break;
			}
		}

		// Mark the PipeReader as complete
		await reader.CompleteAsync();

		return db;
	}

	private IEnumerable<int> getIndicesForHash(int hash)
	{
		TextDatabaseHash h = new(hash, -1);
		List<TextDatabaseHash> table = this.hashTable;
		int i = table.BinarySearch(h, TextDatabaseHashComparer.Instance);

		if (i >= 0)
		{
			Debug.Assert(i == table.Count);
			yield break;
		}

		for (i = ~i; i < table.Count; ++i)
		{
			if (table[i].Hash != hash)
				break;

			yield return table[i].Index;
		}
	}

	/// <summary>
	/// Adds the specified URI to the database and returns the index.
	/// </summary>
	/// <param name="uri"></param>
	/// <returns></returns>
	public async ValueTask<int> AddAsync(string uri)
	{
		if (uri == null)
			throw new ArgumentNullException(nameof(uri));

		if (uri.Length > this.maxUriLength)
			throw new ArgumentException("URI is too long.", nameof(uri));

		// Sanity check.
		for (int i = 0; i < uri.Length; ++i)
		{
			if (char.IsControl(uri[i]))
				throw new ArgumentException("Invalid character in URI.", nameof(uri));
		}

		// TODO: This should really be a read-lock that gets upgraded to write-lock if a new item needs to be added.
		// Alas, there is no upgradable async rwlock class.
		using var _ = await this.rwLock.WriterLockAsync();

		// Find existing.
		int hash = uri.GetHashCode();
		foreach (int i in this.getIndicesForHash(hash))
		{
			if (await this.getAsyncInternal(i) == uri)
				return i;
		}

		// Add new.
		int index = await this.appendAsync(uri);
		return index;
	}

	private async ValueTask<string> getAsyncInternal(int index)
	{
		TextDatabaseRange range = this.lines[index];

		byte[] data = new byte[range.Length];
		int numBytesRead = 0;

		this.stream.Position = range.Offset;
		while (numBytesRead < data.Length)
		{
			int n = await this.stream.ReadAsync(data, numBytesRead, data.Length - numBytesRead);
			if (n == 0)
				throw new EndOfStreamException();

			numBytesRead += n;
		}

		return encoding.GetString(data);
	}

	/// <summary>
	/// Gets the URL at the specified index.
	/// </summary>
	/// <param name="index"></param>
	/// <returns></returns>
	public async ValueTask<string?> GetAsync(int index)
	{
		if (index < 0 || index >= this.Count)
			return null;

		using var _ = await this.rwLock.ReaderLockAsync();

		return await this.getAsyncInternal(index);
	}

	private int addEntry(long offset, int length, int hash)
	{
		Debug.Assert(this.lines.Count == 0 || offset == this.lines[^1].Offset + this.lines[^1].Length + 1);

		int index = this.Count;
		TextDatabaseHash h = new TextDatabaseHash(hash, index);
		int i = this.hashTable.BinarySearch(h, TextDatabaseHashComparer.Instance);
		if (i == this.hashTable.Count)
		{
			this.hashTable.Add(h);
		}
		else if (i < 0)
		{
			this.hashTable.Insert(~i, h);
		}
		else
		{
			throw new InvalidOperationException($"Item {h} already exists in database");
		}

		this.lines.Add(new TextDatabaseRange(offset, length));
		return index;
	}

	private async ValueTask<int> appendAsync(string line)
	{
		byte[] data = encoding.GetBytes(line);
		long offset = this.stream.Length;
		this.stream.Position = this.stream.Length;
		await this.stream.WriteAsync(data);
		await this.stream.WriteAsync(newLine);
		await this.stream.FlushAsync();

		int index = this.addEntry(offset, data.Length, line.GetHashCode());
		return index;
	}

	public void Dispose()
	{
		this.stream?.Dispose();
	}

	#endregion
}

internal sealed class TextDatabaseHashComparer : IComparer<TextDatabaseHash>
{
	public static TextDatabaseHashComparer Instance { get; } = new();

	public int Compare(TextDatabaseHash x, TextDatabaseHash y)
	{
		int hashComparison = x.Hash.CompareTo(y.Hash);
		if (hashComparison != 0)
			return hashComparison;
		return x.Index.CompareTo(y.Index);
	}
}