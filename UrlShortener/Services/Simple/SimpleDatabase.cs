﻿using System.Numerics;
using System.Text;
using FPE;

namespace UrlShortener.Services.Simple;

public class SimpleDatabase : IShortUrlDatabase
{
	private readonly TextDatabase db;
	private readonly string chars;
	private readonly byte[]? tweak;
	private readonly int minLength;
	private readonly int maxLength;
	private readonly FF1 ff1;

	public SimpleDatabase(TextDatabase db, string chars, byte[] key, byte[]? tweak = null, int minLength = 1, int maxLength = 32)
	{
		if (minLength < 1)
			throw new ArgumentOutOfRangeException(nameof(minLength));

		if (maxLength > 32)
			throw new ArgumentOutOfRangeException(nameof(maxLength));

		if (minLength > maxLength)
			throw new ArgumentException("minLength must be less than or equal to maxLength", nameof(minLength));

		this.db = db;
		this.chars = chars;
		this.tweak = tweak;
		this.minLength = minLength;
		this.maxLength = maxLength;
		this.ff1 = new FF1(new AesCipher(key), chars.Length);
	}

	public async ValueTask<string?> ResolveAsync(string shortUrlId)
	{
		if (string.IsNullOrEmpty(shortUrlId) || shortUrlId.Length < this.minLength)
			return null;

		if (shortUrlId.Length > this.maxLength)
			return null;

		RadixString str = new RadixString(shortUrlId, this.chars);
		INumeralString decrypted = this.ff1.Decrypt(str, this.tweak);
		BigInteger id = decrypted.ToBigInteger();
		if (id < 0 || id > int.MaxValue)
			return null; // Out of range for our measly database.

		return await this.db.GetAsync((int)id);
	}

	public async ValueTask<string> AddAsync(string longUrl)
	{
		int id = await this.db.AddAsync(longUrl);
		string encoded = Radix.IntegerToString(id, this.chars, this.minLength);
		RadixString str = new RadixString(encoded, this.chars);
		INumeralString encrypted = this.ff1.Encrypt(str, this.tweak);

		StringBuilder res = new StringBuilder(encrypted.Count);
		foreach (int value in encrypted)
			res.Append(this.chars[value]);

		return res.ToString();
	}
}