﻿using System.Collections;
using System.Numerics;
using FPE;

namespace UrlShortener.Services.Simple;

public readonly struct RadixString : INumeralString
{
	private readonly ReadOnlyMemory<char> str;
	private readonly string chars;

	public int Count => this.str.Length;

	public int this[int index] => this.chars.IndexOf(this.str.Span[index]);

	public RadixString(ReadOnlyMemory<char> str, string chars)
	{
		this.str = str;
		this.chars = chars;
	}

	public RadixString(string str, string chars)
		: this(str.AsMemory(), chars)
	{
	}

	public IEnumerator<int> GetEnumerator()
	{
		for (int i = 0; i < this.Count; ++i)
			yield return this[i];
	}

	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	public BigInteger ToBigInteger()
	{
		return Radix.StringToBigInteger(this.str.Span, this.chars);
	}

	public (INumeralString, INumeralString) Split(int pos)
	{
		return (new RadixString(this.str[..pos], this.chars), new RadixString(this.str[pos..], this.chars));
	}
}