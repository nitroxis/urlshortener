﻿using System.Numerics;
using System.Text;

namespace UrlShortener.Services.Simple;

public static class Radix
{
	private static void leftPad(StringBuilder str, char c, int minLength)
	{
		while (str.Length < minLength)
		{
			str.Insert(0, c);
		}
	}

	public static BigInteger StringToBigInteger(ReadOnlySpan<char> str, ReadOnlySpan<char> chars)
	{
		if (str.Length == 0)
			throw new ArgumentException("Invalid string", nameof(str));

		BigInteger res = BigInteger.Zero;

		int radix = chars.Length;
		for (int i = 0; i < str.Length; ++i)
		{
			char c = str[i];
			int val = chars.IndexOf(c);
			if (val < 0)
				throw new ArgumentException($"Invalid character at position {i}: '{c}' is not in the list of characters.");

			res *= radix;
			res += val;
		}

		return res;
	}

	public static string IntegerToString(int value, ReadOnlySpan<char> chars, int minLength = 1)
	{
		if (minLength < 1)
			throw new ArgumentException("minLength must be at least 1", nameof(minLength));

		StringBuilder str = new();
		int radix = chars.Length;

		while (value != 0)
		{
			int mod = value % radix;
			value /= radix;

			str.Insert(0, chars[mod]);
		}

		leftPad(str, chars[0], minLength);

		return str.ToString();
	}

	public static string BigIntegerToString(BigInteger value, ReadOnlySpan<char> chars, int minLength = 1)
	{
		if (minLength < 1)
			throw new ArgumentException("minLength must be at least 1", nameof(minLength));

		StringBuilder str = new();
		BigInteger radix = chars.Length;

		while (!value.IsZero)
		{
			value = BigInteger.DivRem(value, radix, out BigInteger mod);

			str.Insert(0, chars[(int)mod]);
		}

		leftPad(str, chars[0], minLength);

		return str.ToString();
	}
}