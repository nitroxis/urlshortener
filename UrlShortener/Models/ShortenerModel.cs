﻿using System.ComponentModel.DataAnnotations;

namespace UrlShortener.Models;

public class ShortenerModel
{
	[Required]
	public string? LongUri { get; set; }

	public string? ShortUri { get; set; }

	public string? Error { get; set; }

	public bool RequiresPassword { get; set; }

	public string? Password { get; set; }
}