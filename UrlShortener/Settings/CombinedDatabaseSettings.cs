﻿using UrlShortener.Services;

namespace UrlShortener.Settings;

public class CombinedDatabaseSettings : DatabaseSettings
{
	public DatabaseSettings? Primary { get; set; }
	public DatabaseSettings? Secondary { get; set; }

	public override async Task<IShortUrlDatabase> BuildAsync(ShortenerSettings settings)
	{
		if (this.Primary == null)
			throw new ArgumentException("Missing 'Primary'", nameof(this.Primary));

		if (this.Secondary == null)
			throw new ArgumentException("Missing 'Secondary'", nameof(this.Secondary));

		IShortUrlDatabase primary = await this.Primary.BuildAsync(settings);
		IShortUrlDatabase secondary = await this.Secondary.BuildAsync(settings);

		return new CombinedDatabase(primary, secondary);
	}
}