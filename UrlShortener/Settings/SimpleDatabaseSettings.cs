﻿using UrlShortener.Services;
using UrlShortener.Services.Simple;

namespace UrlShortener.Settings;

public class SimpleDatabaseSettings : DatabaseSettings
{
	public string? File { get; set; }

	public string? Key { get; set; }

	public string? Tweak { get; set; }

	public string Characters = "0123456789" +
	                           "abcdefghijklmnopqrstuvwxyz" +
	                           "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public int MinLength { get; set; } = 5;

	public int MaxLength { get; set; } = 5;

	public override async Task<IShortUrlDatabase> BuildAsync(ShortenerSettings settings)
	{
		if (this.File == null)
			throw new ArgumentException("Missing 'File'", nameof(this.File));

		if (this.Key == null)
			throw new ArgumentException("Missing 'Key'", nameof(this.Key));

		if (string.IsNullOrEmpty(this.Characters))
			throw new ArgumentException("Missing 'Characters'", nameof(this.Characters));

		HashSet<char> chars = new HashSet<char>(this.Characters);
		if (chars.Count != this.Characters.Length)
			throw new ArgumentException("'Characters' must all be unique.", nameof(this.Characters));

		byte[] key = Convert.FromHexString(this.Key);

		byte[]? tweak = string.IsNullOrEmpty(this.Tweak) ? null : Convert.FromHexString(this.Tweak);

		FileStream fs = new FileStream(this.File, FileMode.OpenOrCreate, FileAccess.ReadWrite);
		TextDatabase tdb = await TextDatabase.ParseAsync(fs, settings.MaxUriLength);

		return new SimpleDatabase(tdb, this.Characters, key, tweak, this.MinLength, this.MaxLength);
	}
}