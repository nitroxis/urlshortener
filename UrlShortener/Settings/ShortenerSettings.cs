﻿namespace UrlShortener.Settings;

public class ShortenerSettings
{
	public int MaxUriLength { get; set; } = 10000;
	public List<string> AllowedSchemes { get; set; } = new() { "http", "https" };
	public List<ShortenerPassword> Passwords { get; set; } = new();
}