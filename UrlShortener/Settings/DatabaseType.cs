﻿namespace UrlShortener.Settings;

public enum DatabaseType
{
	Legacy,
	Simple,
	Combined,
}