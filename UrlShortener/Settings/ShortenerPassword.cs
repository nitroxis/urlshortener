﻿using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace UrlShortener.Settings;

public class ShortenerPassword
{
	private const int numBytes = 256 / 8;

	public string? Password { get; set; }
	public string? Salt { get; set; }
	public int IterationCount { get; set; } = 64 * 1024;
	public KeyDerivationPrf Prf { get; set; } = KeyDerivationPrf.HMACSHA256;

	public ShortenerPassword()
	{
	}

	public ShortenerPassword(string password)
	{
		byte[] salt = new byte[128 / 8];
		using (var rng = RandomNumberGenerator.Create())
		{
			rng.GetBytes(salt);
		}

		this.Password = Convert.ToBase64String(KeyDerivation.Pbkdf2(password, salt, this.Prf, this.IterationCount, numBytes));
		this.Salt = Convert.ToBase64String(salt);
	}

	public bool Verify(string? input)
	{
		if (string.IsNullOrEmpty(input) || this.Password == null || this.Salt == null)
			return false;

		byte[] pw = Convert.FromBase64String(this.Password);
		byte[] salt = Convert.FromBase64String(this.Salt);
		byte[] pw2 = KeyDerivation.Pbkdf2(input, salt, this.Prf, this.IterationCount, numBytes);

		return pw.SequenceEqual(pw2);
	}
}