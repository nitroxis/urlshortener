﻿using UrlShortener.Services;

namespace UrlShortener.Settings;

public abstract class DatabaseSettings
{
	public abstract Task<IShortUrlDatabase> BuildAsync(ShortenerSettings settings);
}