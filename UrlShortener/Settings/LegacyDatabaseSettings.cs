﻿using UrlShortener.Services;
using UrlShortener.Services.Legacy;

namespace UrlShortener.Settings;

public class LegacyDatabaseSettings : DatabaseSettings
{
	public string? File { get; set; }

	public override async Task<IShortUrlDatabase> BuildAsync(ShortenerSettings settings)
	{
		if (this.File == null)
			throw new ArgumentException("Missing 'File'", nameof(this.File));

		FileStream fs = new FileStream(this.File, FileMode.OpenOrCreate, FileAccess.ReadWrite);
		TextDatabase tdb = await TextDatabase.ParseAsync(fs, settings.MaxUriLength);
		return new LegacyDatabase(tdb);
	}
}