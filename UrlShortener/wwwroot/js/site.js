﻿function copy(text) {
	const tempInput = $("<input>");
	$("body").append(tempInput);

	tempInput.val(text).select();
	document.execCommand("copy");

	tempInput.remove();
}
