﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using UrlShortener.Models;
using UrlShortener.Services;
using UrlShortener.Settings;

namespace UrlShortener.Controllers;

public class HomeController : Controller
{
	#region Fields

	private readonly IShortUrlDatabase db;
	private readonly ShortenerSettings settings;

	#endregion

	#region Properties

	#endregion

	#region Constructors

	public HomeController(IShortUrlDatabase db, IOptions<ShortenerSettings> settings)
	{
		this.db = db;
		this.settings = settings.Value;
	}

	#endregion

	#region Methods

	private async ValueTask<(string?, string?)> shorten(string? longUri, string? pw)
	{
		string? error = null;

		if (this.settings.Passwords.Count > 0 && !this.settings.Passwords.Any(p => p.Verify(pw)))
		{
			error = "Invalid password.";
		}
		else if (string.IsNullOrWhiteSpace(longUri))
		{
			error = "No URI specified.";
		}
		else if (longUri.Length > this.settings.MaxUriLength)
		{
			error = "URI too long.";
		}
		else if (!Uri.TryCreate(longUri, UriKind.Absolute, out Uri? uri))
		{
			error = "Invalid URI.";
		}
		else if (!this.settings.AllowedSchemes.Contains(uri.Scheme))
		{
			error = $"URI scheme \"{uri.Scheme}\" is not allowed.";
		}

		if (error != null)
			return (null, error);

		string str = await this.db.AddAsync(longUri!);
		return (this.buildAbsoluteUrl(str), null);
	}

	private string buildAbsoluteUrl(string id)
	{
		string? scheme = null;

		if (this.HttpContext.Request.Headers.TryGetValue("X-Forwarded-Proto", out StringValues values))
		{
			scheme = values.FirstOrDefault();
		}

		if (string.IsNullOrWhiteSpace(scheme))
		{
			scheme = this.HttpContext.Request.Scheme;
		}

		return $"{scheme}://{this.HttpContext.Request.Host}/{id}";
	}

	public IActionResult Index()
	{
		return this.View(new ShortenerModel { RequiresPassword = this.settings.Passwords.Count > 0 });
	}

	[HttpPost]
	public async Task<IActionResult> Index(ShortenerModel model)
	{
		(model.ShortUri, model.Error) = await this.shorten(model.LongUri, model.Password);
		model.RequiresPassword = this.settings.Passwords.Count > 0;
		return this.View(model);
	}

	[HttpGet("shorten")]
	public async Task<IActionResult> Shorten(string url, string? pw = null)
	{
		(string? shortUri, string? error) = await this.shorten(url, pw);

		if (error != null)
			return this.BadRequest(error);

		return this.Content(shortUri!, "text/plain");
	}

	[HttpPost("shorten")]
	public async Task<IActionResult> ShortenPost([FromForm] string url, [FromForm] string? pw = null)
	{
		(string? shortUri, string? error) = await this.shorten(url, pw);

		if (error != null)
			return this.BadRequest(error);

		return this.Content(shortUri!, "text/plain");
	}

	[HttpGet("redirect")]
	[HttpGet("{str}")]
	public async Task<IActionResult> RedirectToLongUrl(string str)
	{
		if (string.IsNullOrWhiteSpace(str))
			return this.NotFound();

		if (this.Request.QueryString.HasValue)
			return this.BadRequest();

		string? url = await this.db.ResolveAsync(str);
		if (url == null)
			return this.NotFound();

		return this.RedirectPermanent(url);
	}

	public IActionResult Error()
	{
		return this.View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? this.HttpContext.TraceIdentifier });
	}

	#endregion
}