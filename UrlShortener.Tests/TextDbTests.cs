using System.IO;
using System.Text;
using System.Threading.Tasks;
using UrlShortener.Services;
using Xunit;

namespace UrlShortener.Tests;

public class TextDbTests
{
	private FileStream openTemp() => new(Path.GetTempFileName(), FileMode.Create, FileAccess.ReadWrite, FileShare.None, 4096, FileOptions.DeleteOnClose);

	[Fact]
	public async Task ParseEmpty()
	{
		await using FileStream fs = this.openTemp();

		TextDatabase db = await TextDatabase.ParseAsync(fs, 4096);

		Assert.Equal(0, fs.Length);
		Assert.Equal(0, db.Count);
	}

	[Fact]
	public async Task ParseMissingNewLine()
	{
		await using FileStream fs = this.openTemp();

		await fs.WriteAsync(Encoding.UTF8.GetBytes("test"));
		Assert.Equal(4, fs.Length);

		fs.Position = 0;
		TextDatabase db = await TextDatabase.ParseAsync(fs, 4096);

		Assert.Equal(5, fs.Length); // Should have added a \n at the end.
		Assert.Equal(1, db.Count);
		Assert.Equal("test", await db.GetAsync(0));
	}

	[Fact]
	public async Task ParseLines()
	{
		await using FileStream fs = this.openTemp();

		await fs.WriteAsync(Encoding.UTF8.GetBytes("Test 1\n"));
		await fs.WriteAsync(Encoding.UTF8.GetBytes("Test 2\n"));
		await fs.WriteAsync(Encoding.UTF8.GetBytes("Test 3\n"));
		await fs.WriteAsync(Encoding.UTF8.GetBytes("Test 4\n"));

		fs.Position = 0;
		TextDatabase db = await TextDatabase.ParseAsync(fs, 4096);

		Assert.Equal(4, db.Count);
		Assert.Equal("Test 1", await db.GetAsync(0));
		Assert.Equal("Test 2", await db.GetAsync(1));
		Assert.Equal("Test 3", await db.GetAsync(2));
		Assert.Equal("Test 4", await db.GetAsync(3));
	}

	[Fact]
	public async Task AddExisting()
	{
		await using FileStream fs = this.openTemp();

		await fs.WriteAsync(Encoding.UTF8.GetBytes("Test 1\n"));
		await fs.WriteAsync(Encoding.UTF8.GetBytes("Test 2\n"));

		fs.Position = 0;
		TextDatabase db = await TextDatabase.ParseAsync(fs, 4096);

		Assert.Equal(2, db.Count);
		Assert.Equal("Test 1", await db.GetAsync(0));
		Assert.Equal("Test 2", await db.GetAsync(1));

		Assert.Equal(0, await db.AddAsync("Test 1"));
		Assert.Equal(1, await db.AddAsync("Test 2"));
		Assert.Equal(2, db.Count);

		Assert.Equal(2, await db.AddAsync("Test 3"));
		Assert.Equal(3, db.Count);
	}
}