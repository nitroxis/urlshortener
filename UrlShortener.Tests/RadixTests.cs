﻿using System;
using UrlShortener.Services.Simple;
using Xunit;

namespace UrlShortener.Tests;

public class RadixTests
{
	private const string binary = "01";

	private const string hex = "0123456789abcdef";

	private const string base62 = "0123456789" +
	                              "abcdefghijklmnopqrstuvwxyz" +
	                              "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	[Fact]
	public void FromBinary()
	{
		Assert.Equal(0, Radix.StringToBigInteger("0", binary));
		Assert.Equal(1, Radix.StringToBigInteger("1", binary));
		Assert.Equal(0, Radix.StringToBigInteger("0000", binary));
		Assert.Equal(0b1111, Radix.StringToBigInteger("1111", binary));
		Assert.Equal(0b1000, Radix.StringToBigInteger("1000", binary));
		Assert.Equal(0b1010, Radix.StringToBigInteger("1010", binary));

		Assert.Throws<ArgumentException>(() => Radix.StringToBigInteger("2", binary));
		Assert.Throws<ArgumentException>(() => Radix.StringToBigInteger(" 0 ", binary));
	}

	[Fact]
	public void ToBinary()
	{
		Assert.Equal("0", Radix.BigIntegerToString(0b0, binary));
		Assert.Equal("1", Radix.BigIntegerToString(0b1, binary));
		Assert.Equal("1000", Radix.BigIntegerToString(0b1000, binary));
		Assert.Equal("1111", Radix.BigIntegerToString(0b1111, binary));
	}

	[Fact]
	public void FromHex()
	{
		Assert.Equal(0x0, Radix.StringToBigInteger("0", hex));
		Assert.Equal(0x1, Radix.StringToBigInteger("1", hex));
		Assert.Equal(0xffff, Radix.StringToBigInteger("ffff", hex));
		Assert.Equal(0x1234, Radix.StringToBigInteger("1234", hex));
		Assert.Equal(0x123456789abcdef0, Radix.StringToBigInteger("123456789abcdef0", hex));
	}

	[Fact]
	public void ToHex()
	{
		Assert.Equal("0", Radix.BigIntegerToString(0x0, hex));
		Assert.Equal("1", Radix.BigIntegerToString(0x1, hex));
		Assert.Equal("abc", Radix.BigIntegerToString(0xabc, hex));
		Assert.Equal("ffffffff", Radix.BigIntegerToString(0xffffffff, hex));
	}

	[Fact]
	public void FromBase62()
	{
		Assert.Equal(0, Radix.StringToBigInteger("0", base62));
		Assert.Equal(9, Radix.StringToBigInteger("9", base62));
		Assert.Equal(10, Radix.StringToBigInteger("a", base62));
		Assert.Equal(61 * 62 + 61, Radix.StringToBigInteger("ZZ", base62));
	}

	[Fact]
	public void ToBase62()
	{
		Assert.Equal("0", Radix.BigIntegerToString(0, base62));
		Assert.Equal("9", Radix.BigIntegerToString(9, base62));
		Assert.Equal("a", Radix.BigIntegerToString(10, base62));
		Assert.Equal("ZZ", Radix.BigIntegerToString(61 * 62 + 61, base62));
	}

	[Fact]
	public void MinLength()
	{
		Assert.Equal("0", Radix.BigIntegerToString(0, binary));
		Assert.Equal("000", Radix.BigIntegerToString(0, binary, 3));
		Assert.Equal("00000", Radix.BigIntegerToString(0, binary, 5));
	}
}