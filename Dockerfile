FROM mcr.microsoft.com/dotnet/sdk:6.0 AS restore-env

RUN dotnet tool install -g Microsoft.Web.LibraryManager.Cli
ENV PATH="$PATH:/root/.dotnet/tools"

WORKDIR /app/UrlShortener
COPY UrlShortener/libman.json ./
RUN libman restore

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
WORKDIR /app
COPY --from=restore-env /app .
COPY . ./
RUN dotnet publish UrlShortener -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app
COPY --from=build-env /app/out .
EXPOSE 80
ENTRYPOINT ["dotnet", "UrlShortener.dll"]
